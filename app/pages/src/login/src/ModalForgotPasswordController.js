app.controller('ModalForgotPasswordController', ["$scope", "$uibModal", "$log", "loginService", "loadService", function ($scope, $uibModal, $log, loginService, loadService) {

    "use strict";

    $scope.animationsEnabled = true;

    $scope.open = function (size) {

        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'app/pages/src/login/src/tpl/ModalContentForgotPassword.html',
            controller: function ($scope, $uibModalInstance) {

                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };

                $scope.forgot = function () {
                    loadService.getUsers().then(function (result) {
                        var user1 = result.data.users[0].login;
                        var user2 = result.data.users[1].login;
                        var password1 = result.data.users[0].password;
                        var password2 = result.data.users[1].password;

                        if ($scope.login == user1) {
                            $scope.passwordText = password1;
                        } else if ($scope.login == user2) {
                            $scope.passwordText = password2;
                        } else $scope.passwordText = "Incorect login!"
                    });
                };

            },
            size: size,
            resolve: {
                items: function () {
                    return $scope.items;
                }
            }
        });

        modalInstance.result.then(function (selectedItem) {
            $scope.selected = selectedItem;
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };

    $scope.toggleAnimation = function () {
        $scope.animationsEnabled = !$scope.animationsEnabled;
    };

}]);