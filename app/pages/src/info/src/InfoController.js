app.controller("InfoController", ["$scope", "$state", "loginService", "auth", function ($scope, $state, loginService, auth) {

    "use strict";

    $scope.userInfo = loginService.getUserInfo();

    $scope.logout = function () {
        loginService.logout()
            .then(function (result) {
                $scope.userInfo = null;
                $state.go("login");
            }, function (error) {
                console.log(error);
            });
    };
}]);