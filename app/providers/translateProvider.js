app.config(['$translateProvider', function ($translateProvider) {

    "use strict";

    $translateProvider.useStaticFilesLoader({
        prefix: 'lang/lang-',
        suffix: '.json'
    });

    $translateProvider.preferredLanguage('en');

    $translateProvider.registerAvailableLanguageKeys(['en', 'ru'], {
        'en-US': 'en',
        'ru-RU': 'ru'
    });

    $translateProvider.uniformLanguageTag('bcp47');
    $translateProvider.fallbackLanguage('en');
    $translateProvider.useSanitizeValueStrategy('escape');
    $translateProvider.forceAsyncReload(true);
}]);