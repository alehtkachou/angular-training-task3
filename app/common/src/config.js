app.run(["$rootScope", "$state", function ($rootScope, $state) {

    "use strict";

    $rootScope.$on("$stateChangeSuccess", function (userInfo) {
        console.log(userInfo);
    });

    $rootScope.$on("$stateChangeError", function (event, current, previous, eventObj) {
        if (!eventObj.authenticated) {
            $state.go("login");
        }
    });
}]);